﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Book.Models;
namespace Book.Repository
{
    public class RepoGrqer : IBook
    {
        
        private GradaranEntities gradaranEntities;

        public RepoGrqer(GradaranEntities gradaranEntities)
        {
            this.gradaranEntities = gradaranEntities;
        }

        public void Delete(int grqerid)
        {
            var del = gradaranEntities.Grqers.Find(grqerid);
            gradaranEntities.Grqers.Remove(del);
            gradaranEntities.SaveChanges();
        }

        public Grqer GetGrqerById(int id)
        {
            return gradaranEntities.Grqers.Find(id);
        }

        public IEnumerable<Grqer> GetGrqers()
        {
            return gradaranEntities.Grqers.ToList();
        }

        public void InsertBook(Grqer grqer)
        {
            gradaranEntities.Grqers.Add(grqer);
            gradaranEntities.SaveChanges();
        }

        public void UpdateGrqer(Grqer grqer)
        {
            gradaranEntities.Entry(grqer).State = System.Data.Entity.EntityState.Modified;
            gradaranEntities.SaveChanges();
        }
    }
}