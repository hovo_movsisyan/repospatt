﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Book.Models;

namespace Book.Repository
{
    public interface IBook
    {
        void InsertBook(Grqer grqer);
        IEnumerable<Grqer> GetGrqers();
        void UpdateGrqer(Grqer grqer);
        void Delete(int grqerid);
        Grqer GetGrqerById(int id);
    }
}
