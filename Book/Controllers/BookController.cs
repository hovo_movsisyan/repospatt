﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Book.Models;
using Book.Repository;

namespace Book.Controllers
{
    public class BookController : Controller
    {
        private IBook book;
        public BookController()
        {
            this.book = new RepoGrqer(new GradaranEntities());
        }
        // GET: Book
        public ActionResult Index()
        {
            var grqer = book.GetGrqers().ToList();
            return View(grqer);
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            var gn = book.GetGrqerById(id);
            var display = new Grqer();
            display.id = gn.id;
            display.vernagir = gn.vernagir;
            display.heghinak = gn.heghinak;
            display.gin = gn.gin;
            display.qanak = gn.qanak;
            return View(display);
        }

        // GET: Book/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View(new Grqer());
        }

        // POST: Book/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection,Grqer grqer)
        {
            try
            {
                // TODO: Add insert logic here
                var addgrqer = new Grqer();
                addgrqer.vernagir = grqer.vernagir;
                addgrqer.heghinak = grqer.heghinak;
                addgrqer.gin = grqer.gin;
                addgrqer.qanak = grqer.qanak;
                book.InsertBook(addgrqer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            var gn = book.GetGrqerById(id);
            var display = new Grqer();
            display.id = gn.id;
            display.vernagir = gn.vernagir;
            display.heghinak = gn.heghinak;
            display.gin = gn.gin;
            display.qanak = gn.qanak;
            return View(display);
        }

        // POST: Book/Edit/5
        [HttpPost]
        public ActionResult Edit(Grqer grqer, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                book.UpdateGrqer(grqer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int id)
        {
            var del = book.GetGrqerById(id);
            return View(del);
        }

        // POST: Book/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                book.Delete(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
